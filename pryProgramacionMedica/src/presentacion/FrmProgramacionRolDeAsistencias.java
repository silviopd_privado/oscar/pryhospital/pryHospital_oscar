package presentacion;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;
import negocio.DptoHospital;
import negocio.ProgramacionMedicaAsistencias;
import negocio.Servicios;
import negocio.DetalleProgramacionMedica;
import util.Funciones;

public class FrmProgramacionRolDeAsistencias extends javax.swing.JInternalFrame {

    public int accion = 0;
    public int mes = 0;
    public int CantidadDias = 0;
    public String year;
    public String mes2;
    int total=0;
    
    public FrmProgramacionRolDeAsistencias() {
        initComponents();
        llenarComboDpto();
        cargarFecha();
    }
    
    private boolean validarDatos(){
        if(this.txtDesde.getDate() == null){
            Funciones.mensajeAdvertencia("Debe ingresar LA FECHA", "¡VERIFIQUE!");
            this.txtDesde.requestFocus();
            return false;
        }else if(this.cboDptoHosp.getSelectedItem()== null){
            Funciones.mensajeAdvertencia("Debe seleccionar un DEPARTAMENTO",  "¡VERIFIQUE!");
            this.cboDptoHosp.requestFocus();
            return false;
        }else if(this.cboServicios.getSelectedItem()== null){
            Funciones.mensajeAdvertencia("Debe seleccionar un SERVICIO",  "¡VERIFIQUE!");
            this.cboServicios.requestFocus();
            return false;
        }else if(this.lblIdMedico.getText().isEmpty()){
            Funciones.mensajeAdvertencia("Debe seleccionar un MEDICO", "¡VERIFIQUE!");
            this.btnBuscarMedico.doClick();
            return false;
        } else if(this.txtN_HorasTotal.getText().isEmpty()){
            Funciones.mensajeAdvertencia("No se puede guardar un ROL si su TOTAL DE HORAS = 0", "¡VERIFIQUE!");
            return false;
        }else if(this.tblRolDeAsistencias.getRowCount()==0){
            Funciones.mensajeAdvertencia("Debe ingresar ACTIVIDADES AL ROL", "¡VERIFIQUE!");
            this.btnGuardar.requestFocus();
            return false;
        }        
        return true;
    }

    private void cargarFecha() {
        Date nuev = new Date();
        txtDesde.setDate(nuev);
        PropertyChangeEvent evt = null;
        this.txtDesdePropertyChange(evt);
        mes = txtDesde.getDate().getMonth();
    }

    private void llenarComboDpto() {
        try {
            new DptoHospital().llenarCombo(cboDptoHosp);
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), "¡ERROR!");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel6 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        txtDesde = new com.toedter.calendar.JDateChooser();
        listarMES = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        cboServicios = new javax.swing.JComboBox<>();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        cboDptoHosp = new javax.swing.JComboBox<>();
        jPanel4 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lblDni = new javax.swing.JTextField();
        lblNombres = new javax.swing.JTextField();
        lblColegiatura = new javax.swing.JTextField();
        btnBuscarMedico = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        lblRne = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        lblIdMedico = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        btnQuitar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        /*parte 1*/
        final JTextField field = new JTextField("usat");
        final DefaultCellEditor edit = new DefaultCellEditor(field);
        field.setBorder(BorderFactory.createMatteBorder(3,3,3,3,Color.magenta));
        field.setForeground(Color.blue);
        /*parte 1*/
        tblRolDeAsistencias = new javax.swing.JTable(){
            /*parte 2*/
            public boolean isCellEditable(int fila, int columna){
                if (columna == 0 || columna == 1 || columna == 2||fila==0){
                    return false;
                }
                return true;
            }

            public TableCellEditor getCellEditor(int row, int col) {
                if (col == 2){
                    field.setDocument(new util.ValidaNumeros());
                }else{
                    field.setDocument(new util.ValidaNumeros(util.ValidaNumeros.ACEPTA_DECIMAL));
                }
                edit.setClickCountToStart(2);
                field.addFocusListener(new FocusAdapter() {
                    public void focusLost(FocusEvent e) {
                        field.select(0,0);
                    }
                });
                return edit;
            }
            /*parte 2*/};
        jButton5 = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        lblTotalHoras = new javax.swing.JLabel();
        txtN_HorasTotal = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setTitle("Programación de Rol de Asistencias");

        jPanel6.setBackground(new java.awt.Color(70, 99, 138));

        jPanel1.setBackground(new java.awt.Color(247, 254, 255));

        jLabel1.setFont(new java.awt.Font("Brush Script MT", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("Programación de Horarios del Servicio Asistencial");

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/hosp_las_mercedes - copia.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(254, 254, 254)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(131, 131, 131)
                .addComponent(jLabel3)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel1)))
                .addGap(12, 12, 12))
        );

        jPanel7.setBackground(new java.awt.Color(247, 254, 255));

        jPanel3.setBackground(new java.awt.Color(247, 254, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "FECHA:", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Maiandra GD", 1, 14), new java.awt.Color(0, 153, 153))); // NOI18N

        txtDesde.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                txtDesdePropertyChange(evt);
            }
        });

        listarMES.setBackground(new java.awt.Color(247, 254, 255));
        listarMES.setText("Listar Fecha");
        listarMES.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listarMESActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtDesde, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(listarMES)
                .addContainerGap(12, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(listarMES)
                    .addComponent(txtDesde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 11, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(247, 254, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATOS DEL ÁREA", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Maiandra GD", 1, 14), new java.awt.Color(0, 153, 153))); // NOI18N

        cboServicios.setBackground(new java.awt.Color(247, 254, 255));
        cboServicios.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel12.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel12.setText("DEPARTAMENTO:");

        jLabel13.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel13.setText("SERVICIO:");
        jLabel13.setToolTipText("");

        cboDptoHosp.setBackground(new java.awt.Color(247, 254, 255));
        cboDptoHosp.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cboDptoHosp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboDptoHospActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addGap(18, 18, 18)
                .addComponent(cboDptoHosp, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel13)
                .addGap(18, 18, 18)
                .addComponent(cboServicios, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(221, 221, 221))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(cboServicios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cboDptoHosp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(247, 254, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATOS DEL MÉDICO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Maiandra GD", 1, 14), new java.awt.Color(0, 153, 153))); // NOI18N

        jLabel6.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel6.setText("NOMBRES:");

        jLabel5.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel5.setText("DNI:");

        jLabel11.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel11.setText("COLEGIATURA:");
        jLabel11.setToolTipText("");

        lblDni.setEditable(false);
        lblDni.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblDni.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        lblNombres.setEditable(false);
        lblNombres.setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        lblNombres.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        lblColegiatura.setEditable(false);
        lblColegiatura.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblColegiatura.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        btnBuscarMedico.setBackground(new java.awt.Color(247, 254, 255));
        btnBuscarMedico.setFont(new java.awt.Font("Maiandra GD", 1, 14)); // NOI18N
        btnBuscarMedico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/proveedor.png"))); // NOI18N
        btnBuscarMedico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarMedicoActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel10.setText("RNE:");

        lblRne.setEditable(false);
        lblRne.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblRne.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel7.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        jLabel7.setText("ID:");

        lblIdMedico.setEditable(false);
        lblIdMedico.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblIdMedico.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(lblIdMedico, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDni, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(lblColegiatura, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(lblRne, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(lblNombres, javax.swing.GroupLayout.PREFERRED_SIZE, 538, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnBuscarMedico)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(jLabel7))
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(btnBuscarMedico)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel6)
                                .addComponent(jLabel10)
                                .addComponent(jLabel11))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblColegiatura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblRne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblNombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblDni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblIdMedico, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(247, 254, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "AGREGAR ACTIVIDADES AL ROL", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Maiandra GD", 1, 14), new java.awt.Color(0, 153, 153))); // NOI18N

        btnQuitar.setBackground(new java.awt.Color(247, 254, 255));
        btnQuitar.setFont(new java.awt.Font("Maiandra GD", 1, 10)); // NOI18N
        btnQuitar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/anular.png"))); // NOI18N
        btnQuitar.setText("QUITAR");
        btnQuitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarActionPerformed(evt);
            }
        });

        tblRolDeAsistencias.setBackground(new java.awt.Color(247, 254, 255));
        tblRolDeAsistencias.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {}
            },
            new String [] {

            }
        ));
        tblRolDeAsistencias.setShowVerticalLines(false);
        tblRolDeAsistencias.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tblRolDeAsistenciasPropertyChange(evt);
            }
        });
        tblRolDeAsistencias.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tblRolDeAsistenciasKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(tblRolDeAsistencias);
        /*parte 3*/
        tblRolDeAsistencias.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                field.setText("");
                field.requestFocus();
            }
        });

        field.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode()==10){
                    if (field.getText().isEmpty()){
                        evt.consume();
                    }
                }

            }
        });

        jButton5.setBackground(new java.awt.Color(247, 254, 255));
        jButton5.setFont(new java.awt.Font("Maiandra GD", 1, 10)); // NOI18N
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/add2.png"))); // NOI18N
        jButton5.setText("AGREGAR ACTIVIDAD");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        btnGuardar.setBackground(new java.awt.Color(247, 254, 255));
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ok.png"))); // NOI18N
        btnGuardar.setText("GUARDAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        lblTotalHoras.setFont(new java.awt.Font("Maiandra GD", 1, 12)); // NOI18N
        lblTotalHoras.setText("N.Horas Total:");

        txtN_HorasTotal.setEditable(false);
        txtN_HorasTotal.setBackground(new java.awt.Color(247, 254, 255));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(lblTotalHoras)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtN_HorasTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton5)
                .addGap(18, 18, 18)
                .addComponent(btnQuitar)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnQuitar)
                    .addComponent(jButton5)
                    .addComponent(btnGuardar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTotalHoras)
                    .addComponent(txtN_HorasTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cboDptoHospActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboDptoHospActionPerformed
        if (this.cboDptoHosp.getSelectedIndex() >= 0) {
            int codigoDpto = DptoHospital.listaDptoHosp.get(cboDptoHosp.getSelectedIndex()).getCodigoDptoHosp();
            try {
                new Servicios().cargarComboParaDpto(cboServicios, codigoDpto);
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "¡ERROR!");
            }
        }
    }//GEN-LAST:event_cboDptoHospActionPerformed

    private void btnBuscarMedicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarMedicoActionPerformed
        FrmBuscarMedico objFrm = new FrmBuscarMedico(null, true);
        objFrm.setTitle("Buscar Médico");
        objFrm.cargarListaMedico("", "", "", 0, 0);
        objFrm.setVisible(true);

        int accioResultado = objFrm.accion;
        if (accioResultado == 0) {
            return;
        }
        int filaSeleccionada = objFrm.tblResultado.getSelectedRow();

        String idMedico = objFrm.tblResultado.getValueAt(filaSeleccionada, 0).toString();
        String dni = objFrm.tblResultado.getValueAt(filaSeleccionada, 1).toString();
        String apellidoPaterno = objFrm.tblResultado.getValueAt(filaSeleccionada, 2).toString();
        String apellidoMaterno = objFrm.tblResultado.getValueAt(filaSeleccionada, 3).toString();
        String nombres = objFrm.tblResultado.getValueAt(filaSeleccionada, 4).toString();
        String colegiatura = objFrm.tblResultado.getValueAt(filaSeleccionada, 5).toString();
        String rne = objFrm.tblResultado.getValueAt(filaSeleccionada, 6).toString();

        lblIdMedico.setText(idMedico);
        lblDni.setText(dni);
        lblNombres.setText(apellidoPaterno + " " + apellidoMaterno + " " + nombres);
        lblColegiatura.setText(colegiatura);
        lblRne.setText(rne);
    }//GEN-LAST:event_btnBuscarMedicoActionPerformed
    public void reiniciarJTable(javax.swing.JTable Tabla) {

        DefaultTableModel modelo = (DefaultTableModel) Tabla.getModel();
      
        while (modelo.getRowCount() > 0) {
            modelo.removeRow(0);
        }

        TableColumnModel modCol = Tabla.getColumnModel();
        
        while (modCol.getColumnCount() > 0) {
            modCol.removeColumn(modCol.getColumn(0));
        }
        
//        for (int i = 0; i < 10; i++) {
//            this.tblRolDeAsistencias.getColumnModel().removeColumn(column);
//        }
    }
    
    
    
    
    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
//        if(this.tblRolDeAsistencias.getRowCount() == 0 && this.tblRolDeAsistencias.getRowCount() == -1){
//            Funciones.mensajeAdvertencia("Debe Listar la FECHA", "¡VERIFIQUE!");
//            this.listarMES.doClick();            
//            return;
//      }
        
        if (this.tblRolDeAsistencias.getRowCount()>=7) {
            Funciones.mensajeError("No se puede tener mas de 6 actividades por registro de asistencia", "NO PERMITIDO POR CANTIDAD");
            return;
        }
        FrmBuscarActividades objFrm = new FrmBuscarActividades(null, true);
        objFrm.setTitle("Buscar Otras Actividades");
        objFrm.setVisible(true);
        
        if (objFrm.accion == 1) {
            int fila = objFrm.tblResultado.getSelectedRow();
            String codigoActividad = (objFrm.tblResultado.getValueAt(fila, 0).toString());
//            int codigoServicio = Integer.parseInt(objFrm.tblResultado.getValueAt(fila, 2).toString());
//            int codigoTipoServicio = Integer.parseInt(objFrm.tblResultado.getValueAt(fila, 0).toString());
            String nombreActividad = objFrm.tblResultado.getValueAt(fila, 2).toString();
//            String nombreServicio = objFrm.tblResultado.getValueAt(fila, 3).toString();
//            String nombreTipoServicio = objFrm.tblResultado.getValueAt(fila, 5).toString();
boolean primerValor;
            if (tblRolDeAsistencias.getRowCount()>1) {
                boolean Existe=buscarRepetido(nombreActividad);
            if (Existe) {
                Funciones.mensajeError("Ya existe la actividad en la tabla de programacion", "¡ERROR!");
                return;
            }
            }
            listarMES.doClick();
            llenarTablaActividades(codigoActividad, nombreActividad, tblRolDeAsistencias);

        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private boolean buscarRepetido(String buscado){
 DefaultTableModel modelo = (DefaultTableModel) this.tblRolDeAsistencias.getModel(); 
        
            for (int fila = 0; fila < modelo.getRowCount(); fila++) {
                    String valor = (String) modelo.getValueAt(fila, 1);
                    if (buscado.equalsIgnoreCase(valor)) {
                        return true; 
                }
            }
            return false;
    }
    
    private boolean validarListarFecha(){
        if(this.tblRolDeAsistencias.getRowCount() == 0 && this.tblRolDeAsistencias.getRowCount() == -1){
            Funciones.mensajeAdvertencia("Debe Listar la FECHA", "¡VERIFIQUE!");
            txtDesde.requestFocus();
            return false;
      }
        return true;
    }
    
    private void llenarTablaActividades(String cod,String Actividad,JTable tabla) {
          DefaultTableModel modelo = (DefaultTableModel) this.tblRolDeAsistencias.getModel();    
         String datosVacios[]= new String[CantidadDias];
        datosVacios[0]=cod;
        datosVacios[1]=Actividad;
        int a=0;
                
            for (int i = 2; i < CantidadDias; i++) {
                datosVacios[i]=String.valueOf(0);
            }
        
        modelo.addRow(datosVacios);
        tabla.setModel(modelo);
    }
    
    
    private void tblRolDeAsistenciasKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblRolDeAsistenciasKeyTyped
            int filaselec = tblRolDeAsistencias.getSelectedRow();
            int columselec = tblRolDeAsistencias.getSelectedColumn();
    }//GEN-LAST:event_tblRolDeAsistenciasKeyTyped

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        
        if(this.validarDatos() == false){
                return; // Detiene el programa
             }
        
        int codigoDep=DptoHospital.listaDptoHosp.get(cboDptoHosp.getSelectedIndex()).getCodigoDptoHosp();
      
        int codigoServicio =Servicios.listaServicios.get(cboServicios.getSelectedIndex()).getIdServicio();
      
        int codigoMed=Integer.parseInt(this.lblIdMedico.getText());
        
        int NumHorasTotales=Integer.parseInt(this.txtN_HorasTotal.getText());
         try {
            //grabar en latabla compra
            ProgramacionMedicaAsistencias objProgramacion=new ProgramacionMedicaAsistencias();
            objProgramacion.setId_medico(codigoMed);
            objProgramacion.setId_servicio(codigoServicio );
            objProgramacion.setId_dpto(codigoDep);
            objProgramacion.setNumhoras_total(NumHorasTotales);
            objProgramacion.setAño(Integer.parseInt(year));
            objProgramacion.setMes(Integer.parseInt(mes2));

            //para grabar en compra detalle
            ArrayList<DetalleProgramacionMedica> listaDetalleProgramacion=new ArrayList<DetalleProgramacionMedica>();
            for (int i = 1; i < tblRolDeAsistencias.getRowCount(); i++) {
                int codigoActividad=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 0).toString());
                int numHoraXmes=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 2).toString());
                int dia_1=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 3).toString());
                int dia_2=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 4).toString());
                int dia_3=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 5).toString());
                int dia_4=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 6).toString());
                int dia_5=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 7).toString());
                int dia_6=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 8).toString());
                int dia_7=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 9).toString());
                int dia_8=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 10).toString());
                int dia_9=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 11).toString());
                int dia_10=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 12).toString());
                int dia_11=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 13).toString());
                int dia_12=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 14).toString());
                int dia_13=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 15).toString());
                int dia_14=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 16).toString());
                int dia_15=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 17).toString());
                int dia_16=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 18).toString());
                int dia_17=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 19).toString());
                int dia_18=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 20).toString());
                int dia_19=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 21).toString());
                int dia_20=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 22).toString());
                int dia_21=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 23).toString());
                int dia_22=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 24).toString());
                int dia_23=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 25).toString());
                int dia_24=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 26).toString());
                int dia_25=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 27).toString());
                int dia_26=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 28).toString());
                int dia_27=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 29).toString());
                int dia_28=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 30).toString());
                int dia_29;
                if (tblRolDeAsistencias.getColumnCount()==32) {
                    dia_29=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 31).toString());
                }else{
                    dia_29=0;
                }
                int dia_30;
                if (tblRolDeAsistencias.getColumnCount()==33) {
                    dia_30=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 32).toString());
                }else{
                    dia_30=0;
                }
                int dia_31;
                
                if (tblRolDeAsistencias.getColumnCount()==34) {
                    dia_31=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 33).toString());
                }else{
                    dia_31=0;
                }
//                if (!(tblRolDeAsistencias.getValueAt(i, 33).toString().equalsIgnoreCase(null))) {
//                    dia_31=Integer.parseInt(tblRolDeAsistencias.getValueAt(i, 33).toString());
//                }else{
//                    dia_31=0;
//                }
                
                DetalleProgramacionMedica objbitemDetais=new DetalleProgramacionMedica();
                objbitemDetais.setId_actividad(codigoActividad);
                objbitemDetais.setNro_horas_totales(numHoraXmes);
                objbitemDetais.setDia_1(dia_1);
                objbitemDetais.setDia_2(dia_2);
                objbitemDetais.setDia_3(dia_3);
                objbitemDetais.setDia_4(dia_4);
                objbitemDetais.setDia_5(dia_5);
                objbitemDetais.setDia_6(dia_6);
                objbitemDetais.setDia_7(dia_7);
                objbitemDetais.setDia_8(dia_8);
                objbitemDetais.setDia_9(dia_9);
                objbitemDetais.setDia_10(dia_10);
                objbitemDetais.setDia_11(dia_11);
                objbitemDetais.setDia_12(dia_12);
                objbitemDetais.setDia_13(dia_13);
                objbitemDetais.setDia_14(dia_14);
                objbitemDetais.setDia_15(dia_15);
                objbitemDetais.setDia_16(dia_16);
                objbitemDetais.setDia_17(dia_17);
                objbitemDetais.setDia_18(dia_18);
                objbitemDetais.setDia_19(dia_19);
                objbitemDetais.setDia_20(dia_20);
                objbitemDetais.setDia_21(dia_21);
                objbitemDetais.setDia_22(dia_22);
                objbitemDetais.setDia_23(dia_23);
                objbitemDetais.setDia_24(dia_24);
                objbitemDetais.setDia_25(dia_25);
                objbitemDetais.setDia_26(dia_26);
                objbitemDetais.setDia_27(dia_27);
                objbitemDetais.setDia_28(dia_28);
                objbitemDetais.setDia_29(dia_29);
                objbitemDetais.setDia_30(dia_30);
                objbitemDetais.setDia_31(dia_31);
                
                listaDetalleProgramacion.add(objbitemDetais);
                objProgramacion.setDetalle_programacion_medica(listaDetalleProgramacion);
            }
            
             
             
            if (objProgramacion.agregar()) {
                Funciones.mensajeInformacion("La programación médica se ha GUARDADO con éxito!", "EXITO");
                dispose();
                accion=1;
            }
        } catch (Exception e) {
            Funciones.mensajeError(e.getMessage(), Funciones.NOMBRE_SOFTWARE);
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void txtDesdePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_txtDesdePropertyChange

    }//GEN-LAST:event_txtDesdePropertyChange

    private void listarMESActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listarMESActionPerformed
        this.reiniciarJTable(tblRolDeAsistencias);
        DefaultTableModel dtm = (DefaultTableModel) this.tblRolDeAsistencias.getModel();

        dtm.setColumnCount(0);
        dtm.setRowCount(0);
        if (this.tblRolDeAsistencias.getRowCount() <= 0) {
            dtm.addRow(new Object[]{"1", "2", ""});
        }
        Calendar cal = new GregorianCalendar(txtDesde.getDate().getYear(), txtDesde.getDate().getMonth(), txtDesde.getDate().getDay());
        cal.set(txtDesde.getDate().getYear(), txtDesde.getDate().getMonth(), txtDesde.getDate().getDay());
        //System.out.println(cal.);
        int primerD = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
        int ultimoD = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        java.sql.Date fechainicial = new java.sql.Date(this.txtDesde.getDate().getTime());
        java.util.Date fecha2 = new java.util.Date(this.txtDesde.getDate().getTime());

        String año = fecha2.toString();
        java.util.Date dat;
        dat = (txtDesde.getDate());


        String ultimoDD = Integer.toString(txtDesde.getCalendar().getActualMaximum(Calendar.DAY_OF_MONTH));
        String primerDD = Integer.toString(txtDesde.getCalendar().getActualMinimum(Calendar.DAY_OF_MONTH));
        String dia = Integer.toString(txtDesde.getCalendar().get(Calendar.DAY_OF_MONTH));
        mes2 = Integer.toString(txtDesde.getCalendar().get(Calendar.MONTH) + 1);
        year = Integer.toString(txtDesde.getCalendar().get(Calendar.YEAR));
        
        String fecha = (year + "-" + mes + "-" + dia);
//        this.casoMesCant(txtDesde.getCalendar().get(Calendar.MONTH) + 1, txtDesde.getCalendar().get(Calendar.YEAR));
//txtFecha.setText(fecha);

        dtm.addColumn("CÓDIGO");
        dtm.addColumn("ACTIVIDAD PROGRAMADA");
        dtm.addColumn("N. HORAS");
        for (int i = 1; i <= Integer.parseInt(ultimoDD); i++) {
            dtm.addColumn(" " + i);

        }


        Calendar cal2 = Calendar.getInstance();
        cal2.set(txtDesde.getCalendar().get(Calendar.YEAR), txtDesde.getCalendar().get(Calendar.MONTH), txtDesde.getCalendar().getActualMinimum(Calendar.DAY_OF_MONTH));

        String[] datos = new String[Integer.parseInt(ultimoDD) + 3];
        for (int i = 0; i < datos.length; i++) {
            datos[i] = null;
        }


        int a = 3;
        datos[0] = "";
        datos[1] = " ";
        datos[2] = "  ";
        String diaIngles = "";
        for (int i = 1; i <= Integer.parseInt(ultimoDD); i++) {
            diaIngles = cal2.getTime().toString().substring(0, 3);
            if (diaIngles.equalsIgnoreCase("Mon")) {
                datos[a] = "Lu";
            } else if (diaIngles.equalsIgnoreCase("Tue")) {
                datos[a] = "Ma";
            } else if (diaIngles.equalsIgnoreCase("Wed")) {
                datos[a] = "Mi";
            } else if (diaIngles.equalsIgnoreCase("thu")) {
                datos[a] = "Ju";
            } else if (diaIngles.equalsIgnoreCase("Fri")) {
                datos[a] = "Vi";
            } else if (diaIngles.equalsIgnoreCase("Sat")) {
                datos[a] = "Sa";
            } else {
                datos[a] = "Do";
            }

            cal2.add(Calendar.DAY_OF_MONTH, 1);
            if (a - 2 == Integer.parseInt(ultimoDD)) {
                break;
            }
            a++;
        }
        for (int i = 0; i < Integer.parseInt(ultimoDD) + 3; i++) {
            dtm.setValueAt(datos[i], 0, i);
        }
        if (Integer.parseInt(ultimoDD) + 3 == 34) {
            try {
                int ancho[] = {0, 190, 70, 30, 30, 30, 30, 30, 30, 30,
                               30,30,30,30,30,30,30,30,30,30,
                               30,30,30,30,30,30,30,30,30,30,
                               30,30,30,30};
                String alineacion[] = {"C","I","D","D","D","D","D","C","I","D",
                                       "D","D","D","D","D","D","D","D","D","D", 
                                       "D","D","D","D","D","D","D","D","D","D",
                                       "D","D","D","D"};
                Funciones.llenarTablaPRG(tblRolDeAsistencias, ancho, alineacion);
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error");
            }
            CantidadDias=34;
        }else{
            if (Integer.parseInt(ultimoDD) + 3 == 33) {
            try {
                int ancho[] = {0, 190, 80, 32, 32, 32, 32, 32, 32, 32,
                               32,32,32,32,32,32,32,32,32,32,
                               32,32,32,32,32,32,32,32,32,32,
                               32,32,32};
                String alineacion[] = {"C","I","D","D","D","D","D","C","I","D",
                                       "D","D","D","D","D","D","D","D","D","D", 
                                       "D","D","D","D","D","D","D","D","D","D",
                                       "D","D","D"};
                Funciones.llenarTablaPRG(tblRolDeAsistencias, ancho, alineacion);
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error");
            }
//            llenarTablaActividades(ultimoDD+3,tblRolDeAsistencias);
            CantidadDias=33;
        }else{
                if (Integer.parseInt(ultimoDD) + 3 == 32) {
            try {
                int ancho[] = {0, 190, 80, 32, 32, 32, 32, 32, 32, 32,
                               32,32,32,32,32,32,32,32,32,32,
                               32,32,32,32,32,32,32,32,32,32,
                               32,32};
                String alineacion[] = {"C","I","D","D","D","D","D","C","I","D",
                                       "D","D","D","D","D","D","D","D","D","D", 
                                       "D","D","D","D","D","D","D","D","D","D",
                                       "D","D"};
                Funciones.llenarTablaPRG(tblRolDeAsistencias, ancho, alineacion);
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error");
            }
            CantidadDias=32;
        }else{
                    try {
                int ancho[] = {0, 190, 80, 34, 34, 34, 34, 34, 34, 34,
                               34,34,34,34,34,34,34,34,34,34,
                               34,34,34,34,34,34,34,34,34,34,
                               34};
                String alineacion[] = {"C","I","D","D","D","D","D","C","I","D",
                                       "D","D","D","D","D","D","D","D","D","D", 
                                       "D","D","D","D","D","D","D","D","D","D",
                                       "D"};
                Funciones.llenarTablaPRG(tblRolDeAsistencias, ancho, alineacion);
            } catch (Exception e) {
                Funciones.mensajeError(e.getMessage(), "Error");
            }
                    CantidadDias=31;
                    
                }
            } 
        }

        tblRolDeAsistencias.getColumnModel().getColumn(0).setMaxWidth(0);
tblRolDeAsistencias.getColumnModel().getColumn(0).setMinWidth(0);
tblRolDeAsistencias.getColumnModel().getColumn(0).setPreferredWidth(0);
        this.tblRolDeAsistencias.setModel(dtm);


    }//GEN-LAST:event_listarMESActionPerformed

    private void tblRolDeAsistenciasPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tblRolDeAsistenciasPropertyChange
                        
        if (evt.getPropertyName().equalsIgnoreCase("tableCellEditor")) {
            int fila = this.tblRolDeAsistencias.getSelectedRow();
            int columna = this.tblRolDeAsistencias.getSelectedColumn();
            int acomular=0;
            int valor=Integer.parseInt(tblRolDeAsistencias.getValueAt(fila, columna).toString());
            if ((!(valor==2||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Capacitación"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Capacitacion", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Interconsulta"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Interconsulta", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Telemedicina"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Telemedicina", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Auditoría"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Auditoría", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Comité"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Comité", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Referencias y Contrareferencias"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Referencias y Contrareferencias", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Supervisión"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Supervisión", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==12||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Emergencia"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 12 horas para Emergencia", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Información, Educación y Comunicación"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Información, Educación y Comunicación", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Investigación"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Investigación", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Junta Médica"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Junta Médica", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Procedimiento Diagnóstico Terapéutico"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 2 horas para Procedimiento Diagnóstico Terapéutico", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0||valor==4)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Visita Médica en Hospitalización"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0, 2 o 4 horas para Visita Médica en Hospitalización", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==0||valor==4)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Consulta externa"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0 o 4 horas para Consulta externa", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0||valor==4||valor==6)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Sala de Operaciones"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0, 2, 4 o 6 horas para Sala de Operaciones", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
            if ((!(valor==2||valor==0||valor==4||valor==6)&&tblRolDeAsistencias.getValueAt(fila, 1).toString().equalsIgnoreCase("Centro Obstétrico"))) {
                        Funciones.mensajeAdvertencia("No se admite valores difrentes de 0, 2, 4 o 6 horas para Centro Obstétrico", "ADVERTENCIA");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                        return;
                    }
                    if (!(valor==2||valor==4||valor==6||valor==12||valor==0)) {
                        Funciones.mensajeError("No se admite valores difrentes a 0 2 4 8 ó 12", "Error");
                        tblRolDeAsistencias.setValueAt(0, fila, columna);
                    }

                    for (int i = 3; i < CantidadDias; i++) {                      
                            acomular +=Integer.valueOf((this.tblRolDeAsistencias.getValueAt(fila, i).toString()));
                    }       

                        this.tblRolDeAsistencias.setValueAt(acomular, fila, 2);
                        total+=Integer.valueOf((this.tblRolDeAsistencias.getValueAt(fila, columna).toString()));
                        this.txtN_HorasTotal.setText(String.valueOf(total));
        }

    }//GEN-LAST:event_tblRolDeAsistenciasPropertyChange

    private void btnQuitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarActionPerformed
               DefaultTableModel modeloTabla = (DefaultTableModel) this.tblRolDeAsistencias.getModel(); // Sirve para capturar todo el contenido de la tabla (filas y columnas)

        int fila = this.tblRolDeAsistencias.getSelectedRow();

        if (fila < 0) {
            Funciones.mensajeError("Debe seleccionar una fila", "Verifique");
            return;
        }

        String nombreActividad = tblRolDeAsistencias.getValueAt(fila, 1).toString();
        int respuesta = Funciones.mensajeConfirmacion("Esta seguro de quitar la ACTIVIDAD: " + nombreActividad, "¡CONFIRMAR!");

        if (respuesta != 0) {
            return;
        }

        modeloTabla.removeRow(fila);
        this.tblRolDeAsistencias.setModel(modeloTabla);
    }//GEN-LAST:event_btnQuitarActionPerformed




    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarMedico;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnQuitar;
    private javax.swing.JComboBox<String> cboDptoHosp;
    private javax.swing.JComboBox<String> cboServicios;
    private javax.swing.JButton jButton5;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTextField lblColegiatura;
    public javax.swing.JTextField lblDni;
    public javax.swing.JTextField lblIdMedico;
    public javax.swing.JTextField lblNombres;
    public javax.swing.JTextField lblRne;
    private javax.swing.JLabel lblTotalHoras;
    private javax.swing.JButton listarMES;
    public javax.swing.JTable tblRolDeAsistencias;
    private com.toedter.calendar.JDateChooser txtDesde;
    private javax.swing.JTextField txtN_HorasTotal;
    // End of variables declaration//GEN-END:variables

    

}
