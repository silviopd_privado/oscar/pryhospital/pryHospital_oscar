package principal;

import javax.swing.UIManager;
import presentacion.FrmInicioSesion;
import presentacion.FrmMenuPrincipalAdm;

public class Inicio {

    public static void main(String[] args) throws Exception{

        new UIManager().setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        new FrmInicioSesion().setVisible(true);
//        new FrmMenuPrincipalAdm().setVisible(true);
    }
    
}
