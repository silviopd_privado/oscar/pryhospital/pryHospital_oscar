package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class Procedimientos extends Conexion {

    private int idProcedimientos;
    private String codigoCpt;
    private String descripcion;

    public static ArrayList<Procedimientos> listaProcedimientos = new ArrayList<Procedimientos>();

    public int getIdProcedimientos() {
        return idProcedimientos;
    }

    public void setIdProcedimientos(int idProcedimientos) {
        this.idProcedimientos = idProcedimientos;
    }

    public String getCodigoCpt() {
        return codigoCpt;
    }

    public void setCodigoCpt(String codigoCpt) {
        this.codigoCpt = codigoCpt;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ResultSet listar() throws Exception {
        String sql = "SELECT *  FROM procedimientos;";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;
    }

    public void cargarDatosTabla() throws Exception {
        String sql = "SELECT *  FROM procedimientos;";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        ResultSet resultado = ejecutarSqlSelectSP(sp);
        listaProcedimientos.clear();
        while (resultado.next()) {
            Procedimientos objCargo = new Procedimientos();
            objCargo.setIdProcedimientos(resultado.getInt("id_procedimientos"));
            objCargo.setDescripcion(resultado.getString("descripcion"));
            listaProcedimientos.add(objCargo);
        }
    }

    public void llenarCombo(JComboBox objComboBox) throws Exception {
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaProcedimientos.size(); i++) {
            Procedimientos item = listaProcedimientos.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }

    public String[] obetenerCamposFiltro() {
        String camposFiltro[] = {"CODIGO_CPT", "DESCRIPCION"};
        return camposFiltro;
    }

    public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('procedimientos') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoIDProcedimientos = resultado.getInt("nc");
            this.setIdProcedimientos(nuevoIDProcedimientos);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO public.procedimientos(\n"
                    + "            id_procedimientos, codigo_cpt, descripcion)\n"
                    + "    VALUES (?, ?, ?);";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getIdProcedimientos());
            spInsertar.setString(2, getCodigoCpt());
            spInsertar.setString(3, getDescripcion());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'procedimientos' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla PROCEDIMIENTOS");
        }
        return true;
    }

    public ResultSet leerDatos(int idProcedimientos) throws Exception {
        String sql = "Select * from procedimientos where id_procedimientos = ?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, idProcedimientos);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }

    public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE public.procedimientos\n"
                + "   SET  codigo_cpt=?, descripcion=?\n"
                + " WHERE id_procedimientos=?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setString(1, this.getCodigoCpt());
        sentencia.setString(2, this.getDescripcion());
        sentencia.setInt(3, this.getIdProcedimientos());

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

    public boolean eliminar(int idCargo) throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE FROM public.procedimientos WHERE id_procedimientos=?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);

        sentencia.setInt(1, idCargo);

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

}
