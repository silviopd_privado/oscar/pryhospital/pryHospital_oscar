package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;
import static negocio.Especialidades.listaEspecialidades;

public class Servicios extends Conexion {

    public int idServicio;
    public String descripcion;
    public int idTipoServicio;
    public int idDpto;

    public static ArrayList<Servicios> listaServicios = new ArrayList<Servicios>();

    public int getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(int idServicio) {
        this.idServicio = idServicio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdTipoServicio() {
        return idTipoServicio;
    }

    public void setIdTipoServicio(int idTipoServicio) {
        this.idTipoServicio = idTipoServicio;
    }

    public int getIdDpto() {
        return idDpto;
    }

    public void setIdDpto(int idDpto) {
        this.idDpto = idDpto;
    }

    private void cargarDatosTabla(int id_tipo_servicio, int id_dpto) throws Exception {
        String sql = "SELECT \n"
                + "  servicio.id_servicio, \n"
                + "  servicio.descripcion, \n"
                + "  tipo_servicio.descripcion as tipo_servicio, \n"
                + "  departamento.descripcion as departamento\n"
                + "FROM \n"
                + "  public.servicio, \n"
                + "  public.tipo_servicio, \n"
                + "  public.departamento\n"
                + "WHERE \n"
                + "  tipo_servicio.id_tipo_servicio = servicio.id_tipo_servicio AND\n"
                + "  departamento.id_dpto = servicio.id_dpto and\n"
                + "  public.tipo_servicio.id_tipo_servicio = ? and \n"
                + "  public.departamento.id_dpto = ?\n"
                + "order by\n"
                + "	2;";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        sp.setInt(1, id_tipo_servicio);
        sp.setInt(2, id_dpto);
        ResultSet resultado = ejecutarSqlSelectSP(sp);

        listaServicios.clear();

        while (resultado.next()) {
            Servicios objServicio = new Servicios();
            objServicio.setIdServicio(resultado.getInt("id_servicio"));
            objServicio.setDescripcion(resultado.getString("descripcion"));
            listaServicios.add(objServicio);
        }
    }

    public void cargarCombo(JComboBox objComboBox, int id_tipo_servicio, int id_dpto) throws Exception {
        cargarDatosTabla(id_tipo_servicio, id_dpto);
        objComboBox.removeAllItems();
        for (int i = 0; i < listaServicios.size(); i++) {
            Servicios item = listaServicios.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }

    public ResultSet listar() throws Exception {
        String sql = "SELECT \n"
                + "  servicio.id_servicio, \n"
                + "  servicio.descripcion, \n"
                + "  tipo_servicio.descripcion as tipo_servicio, \n"
                + "  departamento.descripcion as departamento\n"
                + "FROM \n"
                + "  public.servicio, \n"
                + "  public.tipo_servicio, \n"
                + "  public.departamento\n"
                + "WHERE \n"
                + "  tipo_servicio.id_tipo_servicio = servicio.id_tipo_servicio AND\n"
                + "  departamento.id_dpto = servicio.id_dpto \n"
                + "order by\n"
                + "	2;";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;

    }

    public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('servicio') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoCodigo = resultado.getInt("nc");
            this.setIdServicio(nuevoCodigo);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO public.servicio(\n"
                    + "            id_servicio, descripcion, id_tipo_servicio, id_dpto)\n"
                    + "    VALUES (?, ?, ?, ?);";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getIdServicio());
            spInsertar.setString(2, getDescripcion());
            spInsertar.setInt(3, getIdTipoServicio());
            spInsertar.setInt(4, getIdDpto());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'servicio' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla Especialidades");
        }
        return true;
    }

    public String[] obtenerCamposFiltro() {
        String camposFiltro[] = {"ID_SERVICIO", "DESCRIPCION", "TIPO_SERVICIO", "DEPARTAMENTO"};
        return camposFiltro;
    }

    public ResultSet leerDatos(int idServicio) throws Exception {
        String sql = "Select * from servicio where id_servicio = ?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, idServicio);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }

    public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE public.servicio\n"
                + "   SET  descripcion = ?, id_tipo_servicio = ?, id_dpto = ?\n"
                + " WHERE id_servicio = ?;";

        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setString(1, this.getDescripcion());
        sentencia.setInt(2, this.getIdTipoServicio());
        sentencia.setInt(3, this.getIdDpto());
        sentencia.setInt(4, this.getIdServicio());
        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

    public boolean eliminar(int codigoServicio) throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE from servicio WHERE id_servicio = ?;";

        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setInt(1, codigoServicio);
        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }
    
    private void cargarDatosTablaParaDpto(int id_dpto) throws Exception {
        String sql = "select * from servicio where id_dpto=?";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        sp.setInt(1, id_dpto);
        ResultSet resultado = ejecutarSqlSelectSP(sp);

        listaServicios.clear();

        while (resultado.next()) {
            Servicios objServicio = new Servicios();
            objServicio.setIdServicio(resultado.getInt("id_servicio"));
            objServicio.setDescripcion(resultado.getString("descripcion"));
            listaServicios.add(objServicio);
        }
    }
    
    public void cargarComboParaDpto(JComboBox objComboBox, int codDpto) throws Exception {
        cargarDatosTablaParaDpto(codDpto);
        objComboBox.removeAllItems();
        for (int i = 0; i < listaServicios.size(); i++) {
            Servicios item = listaServicios.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }

}
