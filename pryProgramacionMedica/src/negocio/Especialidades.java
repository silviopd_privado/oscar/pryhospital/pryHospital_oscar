package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class Especialidades extends Conexion {

    private int codigoEspecialidad;
    private String nombre;
    private int codigoDpto;
    private int tiempoAtencion;

    public static ArrayList<Especialidades> listaEspecialidades = new ArrayList<Especialidades>();

    public int getCodigoEspecialidad() {
        return codigoEspecialidad;
    }

    public void setCodigoEspecialidad(int codigoEspecialidad) {
        this.codigoEspecialidad = codigoEspecialidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigoDpto() {
        return codigoDpto;
    }

    public void setCodigoDpto(int codigoDpto) {
        this.codigoDpto = codigoDpto;
    }

    public int getTiempoAtencion() {
        return tiempoAtencion;
    }

    public void setTiempoAtencion(int tiempoAtencion) {
        this.tiempoAtencion = tiempoAtencion;
    }

    private void cargarDatosTabla(int codigoDepartamento) throws Exception {
        String sql = "select * from especialidad where id_dpto = ? order by 2";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        sp.setInt(1, codigoDepartamento);
        ResultSet resultado = ejecutarSqlSelectSP(sp);

        listaEspecialidades.clear();

        while (resultado.next()) {
            Especialidades objEspecialidad = new Especialidades();
            objEspecialidad.setCodigoEspecialidad(resultado.getInt("id_especialidad"));
            objEspecialidad.setNombre(resultado.getString("descripcion"));
            listaEspecialidades.add(objEspecialidad);
        }
    }

    public void cargarCombo(JComboBox objComboBox, int codigoDepartamento) throws Exception {
        cargarDatosTabla(codigoDepartamento);
        objComboBox.removeAllItems();
        for (int i = 0; i < listaEspecialidades.size(); i++) {
            Especialidades item = listaEspecialidades.get(i);
            objComboBox.addItem(item.nombre);
        }
    }

    public ResultSet listar() throws Exception {
        String sql = "select e.id_especialidad as id, e.descripcion, d.descripcion as departamento "
                + " from especialidad e "
                + "inner join departamento d on (e.id_dpto = d.id_dpto)";

//        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
//        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
//        return resultado;
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;

    }

    public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('especialidad') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoCodigo = resultado.getInt("nc");
            this.setCodigoEspecialidad(nuevoCodigo);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO public.especialidad(id_especialidad, descripcion, id_dpto) VALUES (?, ?, ?);";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getCodigoEspecialidad());
            spInsertar.setString(2, getNombre());
            spInsertar.setInt(3, getCodigoDpto());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'especialidad' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla Especialidades");
        }
        return true;
    }

    public String[] obtenerCamposFiltro() {
        String camposFiltro[] = {"ID", "DESCRIPCION", "DEPARTAMENTO"};
        return camposFiltro;
    }

    public ResultSet leerDatos(int codigoEspecialidad) throws Exception {
        String sql = "Select * from especialidad where id_especialidad=?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, codigoEspecialidad);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }

    public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE public.especialidad SET descripcion = ?, id_dpto = ? WHERE id_especialidad = ?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setString(1, this.getNombre());
        sentencia.setInt(2, this.getCodigoDpto());
        sentencia.setInt(3, this.getCodigoEspecialidad());

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

    public boolean eliminar(int codigoEspecialidad) throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE from especialidad WHERE id_especialidad = ?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);

        sentencia.setInt(1, codigoEspecialidad);

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

}
