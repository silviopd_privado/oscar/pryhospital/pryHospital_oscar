package negocio;

import accesoDatos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class TipoServicio extends Conexion {

    public int idTipoServicio;
    public String descripcion;

    public static ArrayList<TipoServicio> listaTipoServicio = new ArrayList<TipoServicio>();

    public int getIdTipoServicio() {
        return idTipoServicio;
    }

    public void setIdTipoServicio(int idTipoServicio) {
        this.idTipoServicio = idTipoServicio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void cargarDatosTabla() throws Exception {
        String sql = "select * from tipo_servicio order by 2";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        ResultSet resultado = ejecutarSqlSelectSP(sp);
        listaTipoServicio.clear();
        while (resultado.next()) {
            TipoServicio objTipoServicio = new TipoServicio();
            objTipoServicio.setIdTipoServicio(resultado.getInt("id_tipo_servicio"));
            objTipoServicio.setDescripcion(resultado.getString("descripcion"));
            listaTipoServicio.add(objTipoServicio);
        }
    }

    public void llenarCombo(JComboBox objComboBox) throws Exception {
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaTipoServicio.size(); i++) {
            TipoServicio item = listaTipoServicio.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }
}
