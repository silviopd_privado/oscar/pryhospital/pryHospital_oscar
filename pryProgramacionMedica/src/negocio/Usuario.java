package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Usuario extends Conexion {

    private int idUsuario;
    private int idTipoUsuario;
    private int idPersonal;
    private String dni;
    private String clave;
    private String estado;

    public static ArrayList<Usuario> listaUsuario = new ArrayList<Usuario>();

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdTipoUsuario() {
        return idTipoUsuario;
    }

    public void setIdTipoUsuario(int idTipoUsuario) {
        this.idTipoUsuario = idTipoUsuario;
    }

    public int getIdPersonal() {
        return idPersonal;
    }

    public void setIdPersonal(int idPersonal) {
        this.idPersonal = idPersonal;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ResultSet listar() throws Exception {
        String sql = "SELECT \n"
                + "  usuario.id_usuario, \n"
                + "  tipo_usuario.descripcion as tipo_usuario, \n"
                + "  personal.dni, \n"
                + "  personal.apellido_paterno, \n"
                + "  personal.apellido_materno, \n"
                + "  personal.nombres, \n"
                + "  personal.sexo, \n"
                + "  personal.estado\n"
                + "FROM \n"
                + "  public.usuario, \n"
                + "  public.tipo_usuario, \n"
                + "  public.personal\n"
                + "WHERE \n"
                + "  usuario.id_tipo_usuario = tipo_usuario.id_tipo_usuario AND\n"
                + "  usuario.id_personal = personal.id_personal;";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;

    }

    public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('usuario') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoCodigo = resultado.getInt("nc");
            this.setIdUsuario(nuevoCodigo);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO public.usuario(\n"
                    + "            id_usuario, id_tipo_usuario, id_personal, dni, clave, estado)\n"
                    + "    VALUES (?, ?, ?, ?, md5(?), ?);";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getIdUsuario());
            spInsertar.setInt(2, getIdTipoUsuario());
            spInsertar.setInt(3, getIdPersonal());
            spInsertar.setString(4, getDni());
            spInsertar.setString(5, getClave());
            spInsertar.setString(6, getEstado());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'usuario' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla Usuario");
        }
        return true;
    }

    public String[] obtenerCamposFiltro() {
        String camposFiltro[] = {"ID_USUARIO", "NOMBRES", "TIPO_USUARIO"};
        return camposFiltro;
    }

    public ResultSet leerDatos(int codigoUsuario) throws Exception {
        String sql = "Select * from usuario where id_usuario=?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, codigoUsuario);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }

    public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE public.usuario\n"
                + "   SET  id_tipo_usuario=?, id_personal=?, dni=?, clave=md5(?), \n"
                + "       estado=?\n"
                + " WHERE id_usuario=?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setInt(1, this.getIdTipoUsuario());
        sentencia.setInt(2, this.getIdPersonal());
        sentencia.setString(3, this.getDni());
        sentencia.setString(4, this.getClave());
        sentencia.setString(5, this.getEstado());
        sentencia.setInt(6, this.getIdUsuario());

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

    public boolean eliminar(int codigoUsuario) throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE from usuario  WHERE id_usuario=?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);

        sentencia.setInt(1, codigoUsuario);

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

}
