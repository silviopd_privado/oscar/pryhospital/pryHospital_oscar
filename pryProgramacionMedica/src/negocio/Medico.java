package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Medico extends Conexion {

    private int idMedico;
    private int idPersonal;
    private String colegiatura;
    private String rne;

    public int getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(int idMedico) {
        this.idMedico = idMedico;
    }

    public int getIdPersonal() {
        return idPersonal;
    }

    public void setIdPersonal(int idPersonal) {
        this.idPersonal = idPersonal;
    }

    public String getColegiatura() {
        return colegiatura;
    }

    public void setColegiatura(String colegiatura) {
        this.colegiatura = colegiatura;
    }

    public String getRne() {
        return rne;
    }

    public void setRne(String rne) {
        this.rne = rne;
    }

    public ResultSet listar() throws Exception {
        String sql = "select \n" +
                "	m.id_medico,\n" +
                "	m.id_personal,\n" +
                "	p.dni,\n" +
                "	(p.apellido_paterno || ' ' || p.apellido_materno || ' ' || p.nombres)::varchar as nombres,\n" +
                "	m.colegiatura,\n" +
                "	m.rne\n" +
                " from medico m inner join personal p on (m.id_personal=p.id_personal);";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;
    }
    
    public ResultSet listar_medico()throws Exception{
            String sql = "select m.id_medico, p.dni, p.apellido_paterno, p.apellido_materno, p.nombres, m.colegiatura, m.rne from personal p "
                    + "inner join medico m on (p.id_personal = m.id_personal)";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;
    }

    public String[] obtenerCamposFiltro() {
        String camposFiltro[] = { "DNI","NOMBRES", "COLEGIATURA", "RNE"};
        return camposFiltro;
    }

    public ResultSet leerDatos(int codigoMedico) throws Exception {
        String sql = "SELECT \n"
                + "  medico.id_medico, \n"
                + "  medico.id_personal, \n"
                + "  medico.colegiatura, \n"
                + "  medico.rne\n"
                + "FROM \n"
                + "  public.medico\n"
                + "WHERE id_medico=?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, codigoMedico);
        ResultSet resutado = this.ejecutarSqlSelectSP(sentencia);
        return resutado;
    }
    public ResultSet litar2(String nombre,String apellidoP,String apellidoM,int mes,int año) throws Exception {
        if (nombre.equals("")) {
            String sql = "select * from f_buscarMedico('','','',0,0)";
            PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet resultado = this.ejecutarSqlSelectSP(sp);
            return resultado;
        }
//        String sql = "select * from f_buscarMedico(?,?,?,?,?)";
//        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
//        sentencia.setString(1,nombre);
//        sentencia.setString(2,apellidoM );
//        sentencia.setString(3,apellidoP );
//        sentencia.setInt(4,mes );
//        sentencia.setInt(5,año );
//        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        
        String sql = "select * from f_buscarMedico(?,?,?,?,?)";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql,ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_READ_ONLY);
        sentencia.setString(1,nombre);
        sentencia.setString(2,apellidoM );
        sentencia.setString(3,apellidoP );
        sentencia.setInt(4,mes );
        sentencia.setInt(5,año );
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
        
        
        //ResultSet resutado = this.ejecutarSqlSelectSP(sentencia);
        //return resultado;
    }

    public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('medico') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoCodigo = resultado.getInt("nc");
            this.setIdMedico(nuevoCodigo);

            sql = "INSERT INTO public.medico(\n"
                    + "            id_medico, id_personal, colegiatura, rne)\n"
                    + "    VALUES (?, ?, ?, ?);";
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);
            PreparedStatement spInsertarMedico = transaccion.prepareStatement(sql);

            spInsertarMedico.setInt(1, getIdMedico());
            spInsertarMedico.setInt(2, getIdPersonal());
            spInsertarMedico.setString(3, getColegiatura());
            spInsertarMedico.setString(4, getRne());

            this.ejecutarSql(spInsertarMedico, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'medico' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);
            /*FIN ACTUALIZAR EL CORRELATIVO*/

            transaccion.commit();
            transaccion.close();

        } else { //Es porque no encuentra el correlativo
            throw new Exception("No se ha encontrado el correlativo para la tabla Medico");
        }
        return true;
    }

    public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE public.medico\n"
                + "   SET  id_personal=?, colegiatura=?, rne=?\n"
                + " WHERE id_medico = ?;";

        PreparedStatement aMedico = transaccion.prepareStatement(sql);
        aMedico.setInt(1, this.getIdPersonal());
        aMedico.setString(2, this.getColegiatura());
        aMedico.setString(3, this.getRne());
        aMedico.setInt(4, this.getIdMedico());

        this.ejecutarSql(aMedico, transaccion);
//
//        sql = "UPDATE public.medico_especialidad SET id_especialidad=?, id_medico=? WHERE id_medico_especialidad=?;";
//
//        PreparedStatement aMedicoEspecialidad = transaccion.prepareStatement(sql);
//        aMedicoEspecialidad.setInt(1, this.getCodigoEspecialidad());
//        aMedicoEspecialidad.setInt(2, this.getCodigoMedico());
//        aMedicoEspecialidad.setInt(3, this.getCodigoMedicoEspecialidad());
//
//        this.ejecutarSql(aMedicoEspecialidad, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

    public boolean eliminar(int codigoMedico) throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE FROM public.medico WHERE id_medico = ?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setInt(1, codigoMedico);
        
//         sql = "update correlativo set numero = numero - 1 where tabla = 'medico' ";
//            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
//            this.ejecutarSql(spActualizar, transaccion);

        this.ejecutarSql(sentencia, transaccion);
        transaccion.commit();
        transaccion.close();

        return true;
    }
}
