package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Observaciones extends Conexion{
    
    private int idObservaciones;
    private int idProgramacionMedica;
    private String descripcion;

    public int getIdObservaciones() {
        return idObservaciones;
    }

    public void setIdObservaciones(int idObservaciones) {
        this.idObservaciones = idObservaciones;
    }

    public int getIdProgramacionMedica() {
        return idProgramacionMedica;
    }

    public void setIdProgramacionMedica(int idProgramacionMedica) {
        this.idProgramacionMedica = idProgramacionMedica;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public boolean agregarObservaciones() throws Exception {
        String sql = "select * from f_generar_correlativo('observaciones') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoIdObservacion = resultado.getInt("nc");
            this.setIdObservaciones(nuevoIdObservacion);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO public.observaciones(id_observaciones, id_programacion_medica, descripcion)\n" +
                        "    VALUES (?, ?, ?);";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getIdObservaciones());
            spInsertar.setInt(2, getIdProgramacionMedica());
            spInsertar.setString(3, getDescripcion());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'observaciones' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla OBSERVACIONES");
        }
        return true;
    }
    
    
    
}
