package negocio;

import accesoDatos.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import util.Funciones;

public class InicioSesion extends Conexion{

    private String dniEmpleado;
    private String clave;
    private int tipoUsuario;
    
    

    public String getDniEmpleado() {
        return dniEmpleado;
    }

    public void setDniEmpleado(String dniEmpleado) {
        this.dniEmpleado = dniEmpleado;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
   
    public int getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(int tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
    
        public int iniciarSesionSP() throws Exception{
        String sql = "select e.apellido_paterno, e.apellido_materno, e.nombres, u.id_usuario, u.estado, t.id_tipo_usuario, t.descripcion as tipo_usuario from personal e \n" +
"                          inner join usuario u on (e.id_personal = u.id_personal) \n" +
"                           inner join tipo_usuario t on (u.id_tipo_usuario = t.id_tipo_usuario) \n" +
"                          where e.dni=? and u.clave = md5(?);"; //esta en criptado en md5 
        
//        String sql = "select e.apellido_paterno, e.apellido_materno, e.nombres, u.id_usuario, u.estado, t.id_tipo_usuario, t.descripcion as tipo_usuario from empleado e "
//                            + "inner join usuario u on (e.dni = u.dni_empleado) "
//                            + "inner join tipo_usuario t on (u.tipo_usuario = t.id_tipo_usuario) "
//                            + "where e.dni=? and u.clave = md5(?);"; //esta en criptado en md5 
        
//        System.out.println(sql);
        
            PreparedStatement sentencia = abrirConexion().prepareStatement(sql);
                sentencia.setString(1, this.getDniEmpleado());
                sentencia.setString(2, this.getClave());
        
        //ResultSet resultado  = this.ejecutarSQLSeleResultSet(sql);
        ResultSet resultado  = this.ejecutarSqlSelectSP(sentencia);
             
        if (resultado.next()){
            if(resultado.getString("estado").equalsIgnoreCase("A")){
                //Captural el nombre y apellidos del usuario que va a ingresar al sistema
                Funciones.USUARIO_LOGUEADO_NOMBRE = resultado.getString("apellido_paterno") + " "  + resultado.getString("apellido_materno") + " " + resultado.getString("nombres") ;
                Funciones.USUARIO_LOGUEADO_CODIGO = resultado.getInt("id_usuario");
                Funciones.CODIGO_TIPO_USUARIO = resultado.getInt("id_tipo_usuario");
                return 1; //El usuario puede regresar al sistema
            }else{
                return 2; //El usuario no puede ingresar al sistema
             } 
        }      
        
         return -1; //Hubo un error al actualizar los datos
    }

        public boolean validarTipoUsuario(int codigoTipoUsuario,String dni)throws Exception{
            String sql = "select * from usuario where id_tipo_usuario=? and dni=?";
            PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
            sentencia.setInt(1, codigoTipoUsuario+1);
            sentencia.setString(2,dni);
//            System.out.println("codigo:"+codigoTipoUsuario+1);
            ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
            
            if(resultado.next()){
                return true;
            }return false;
        }


    
}
