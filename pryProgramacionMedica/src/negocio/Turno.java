package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

public class Turno extends Conexion {

    private int idTurno;
    private String codTurno;
    private String descripcion;
    private String horaInicio;
    private String horaFin;

    public static ArrayList<Turno> listaTurno = new ArrayList<Turno>();

    public int getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(int idTurno) {
        this.idTurno = idTurno;
    }

    public String getCodTurno() {
        return codTurno;
    }

    public void setCodTurno(String codTurno) {
        this.codTurno = codTurno;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    private void cargarDatosTabla() throws Exception {
        String sql = "select * from turno order by 2";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        ResultSet resultado = ejecutarSqlSelectSP(sp);

        listaTurno.clear();

        while (resultado.next()) {
            Turno objEspecialidad = new Turno();
            objEspecialidad.setIdTurno(resultado.getInt("id_turno"));
            objEspecialidad.setDescripcion(resultado.getString("descripcion"));
            listaTurno.add(objEspecialidad);
        }
    }

    public void llenarCombo(JComboBox objComboBox) throws Exception {
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaTurno.size(); i++) {
            Turno item = listaTurno.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }

    public ResultSet listar() throws Exception {
        String sql = "Select * from turno order by 1;";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;

    }

    public String[] obetenerCamposFiltro() {
        String camposFiltro[] = {"DESCRIPCION"};
        return camposFiltro;
    }

    public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('turno') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoCodigoDpto = resultado.getInt("nc");
            this.setIdTurno(nuevoCodigoDpto);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO public.turno(\n"
                    + "            id_turno, cod_turno, descripcion, hora_inicio, hora_fin)\n"
                    + "    VALUES (?, ?, ?, ?, ?);";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getIdTurno());
            spInsertar.setString(2, getCodTurno());
            spInsertar.setString(3, getDescripcion());
            spInsertar.setString(4, getHoraInicio());
            spInsertar.setString(5, getHoraFin());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'turno' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla Turno");
        }
        return true;
    }

    public ResultSet leerDatos(int codigoTurno) throws Exception {
        String sql = "Select * from turno where id_turno = ?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, codigoTurno);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }

    public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE public.turno\n"
                + "   SET  cod_turno=?, descripcion=?, hora_inicio=?, hora_fin=?\n"
                + " WHERE id_turno=?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setString(1, this.getCodTurno());
        sentencia.setString(2, this.getDescripcion());
        sentencia.setString(3, this.getHoraInicio());
        sentencia.setString(4, this.getHoraFin());
        sentencia.setInt(5, this.getIdTurno());

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

    public boolean eliminar(int codigoTurno) throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE FROM public.turno\n"
                + " WHERE id_turno = ?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setInt(1, codigoTurno);
        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }

    public void llenarComboDescripcion(JComboBox objComboBox) throws Exception {
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaTurno.size(); i++) {
            Turno item = listaTurno.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }

    public void llenarComboHoraInicio(JComboBox objComboBox) throws Exception {
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaTurno.size(); i++) {
            Turno item = listaTurno.get(i);
            objComboBox.addItem(item.horaInicio);
        }
    }

    public void llenarComboHoraFin(JComboBox objComboBox) throws Exception {
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaTurno.size(); i++) {
            Turno item = listaTurno.get(i);
            objComboBox.addItem(item.horaFin);
        }
    }

}
