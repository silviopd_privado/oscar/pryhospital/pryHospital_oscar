package negocio;

import accesoDatos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;
import static negocio.Actividad.listaActividades;

public class Cargo extends Conexion{
    
    private int idCargo;
    private String descripcion;
    
    public static ArrayList<Cargo> listaCargo = new ArrayList<Cargo>();

    public int getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(int idCargo) {
        this.idCargo = idCargo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public ResultSet listar()throws Exception{
            String sql = "SELECT id_cargo as id, descripcion  FROM cargo;";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet resultado = this.ejecutarSqlSelectSP(sp);
        return resultado;
    }
    
    public void cargarDatosTabla() throws Exception {
        String sql = "SELECT id_cargo as id, descripcion  FROM cargo;";
        PreparedStatement sp = abrirConexion().prepareStatement(sql);
        ResultSet resultado = ejecutarSqlSelectSP(sp);
        listaCargo.clear();
        while (resultado.next()) {
            Cargo objCargo = new Cargo();
            objCargo.setIdCargo(resultado.getInt("id"));
            objCargo.setDescripcion(resultado.getString("descripcion"));
            listaCargo.add(objCargo);
        }
    }
    
    public void llenarCombo(JComboBox objComboBox) throws Exception {
        cargarDatosTabla();
        objComboBox.removeAllItems();
        for (int i = 0; i < listaCargo.size(); i++) {
            Cargo item = listaCargo.get(i);
            objComboBox.addItem(item.descripcion);
        }
    }
    
    public String[] obetenerCamposFiltro() {
        String camposFiltro[] = {"ID","DESCRIPCION"};
        return camposFiltro;
    }
    
    public boolean agregar() throws Exception {
        String sql = "select * from f_generar_correlativo('cargo') as nc";
        ResultSet resultado = this.ejecutarSqlSelect(sql);
        if (resultado.next()) {
            int nuevoIDCargo = resultado.getInt("nc");
            this.setIdCargo(nuevoIDCargo);

            /* ------------------- INICIA LA TRANSACCION ---------------------*/
            Connection transaccion = this.abrirConexion();

            transaccion.setAutoCommit(false);

            sql = "INSERT INTO public.cargo(id_cargo, descripcion) VALUES (?, ?);;";
            PreparedStatement spInsertar = spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setInt(1, getIdCargo());
            spInsertar.setString(2, getDescripcion());

            this.ejecutarSql(spInsertar, transaccion);

            sql = "update correlativo set numero = numero + 1 where tabla = 'cargo' ";
            PreparedStatement spActualizar = transaccion.prepareStatement(sql);
            this.ejecutarSql(spActualizar, transaccion);

            transaccion.commit();
            transaccion.close();

        } else {
            throw new Exception("No se ha encontrado el correlativo para la tabla CARGO");
        }
        return true;
    }
    
    public ResultSet leerDatos(int IdCargo) throws Exception {
        String sql = "Select * from cargo where id_cargo = ?";
        PreparedStatement sentencia = this.abrirConexion().prepareStatement(sql);
        sentencia.setInt(1, IdCargo);
        ResultSet resultado = this.ejecutarSqlSelectSP(sentencia);
        return resultado;
    }
    
    public boolean editar() throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "UPDATE public.cargo SET descripcion=? WHERE id_cargo=?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setString(1, this.getDescripcion());
        sentencia.setInt(2, this.getIdCargo());

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }
    
    public boolean eliminar(int idCargo )throws Exception {
        Connection transaccion = this.abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "DELETE FROM public.cargo WHERE id_cargo=?;";
        PreparedStatement sentencia = transaccion.prepareStatement(sql);

        sentencia.setInt(1, idCargo);

        this.ejecutarSql(sentencia, transaccion);

        transaccion.commit();
        transaccion.close();

        return true;
    }
    
  
}
