package negocio;

public class DetalleProgramacionMedica {
    private int id_detalle;
    private int id_actividad;
    private int dia_1;
    private int dia_2;
    private int dia_3;
    private int dia_4;
    private int dia_5;
    private int dia_6;
    private int dia_7;
    private int dia_8;
    private int dia_9;
    private int dia_10;
    private int dia_11;
    private int dia_12;
    private int dia_13;
    private int dia_14;
    private int dia_15;
    private int dia_16;
    private int dia_17;
    private int dia_18;
    private int dia_19;
    private int dia_20;
    private int dia_21;
    private int dia_22;
    private int dia_23;
    private int dia_24;
    private int dia_25;
    private int dia_26;
    private int dia_27;
    private int dia_28;
    private int dia_29;
    private int dia_30;
    private int dia_31;
    private int nro_horas_totales;
    private int id_programacion_medica;

    public int getId_detalle() {
        return id_detalle;
    }

    public void setId_detalle(int id_detalle) {
        this.id_detalle = id_detalle;
    }

    public int getId_actividad() {
        return id_actividad;
    }

    public void setId_actividad(int id_actividad) {
        this.id_actividad = id_actividad;
    }

    public int getDia_1() {
        return dia_1;
    }

    public void setDia_1(int dia_1) {
        this.dia_1 = dia_1;
    }

    public int getDia_2() {
        return dia_2;
    }

    public void setDia_2(int dia_2) {
        this.dia_2 = dia_2;
    }

    public int getDia_3() {
        return dia_3;
    }

    public void setDia_3(int dia_3) {
        this.dia_3 = dia_3;
    }

    public int getDia_4() {
        return dia_4;
    }

    public void setDia_4(int dia_4) {
        this.dia_4 = dia_4;
    }

    public int getDia_5() {
        return dia_5;
    }

    public void setDia_5(int dia_5) {
        this.dia_5 = dia_5;
    }

    public int getDia_6() {
        return dia_6;
    }

    public void setDia_6(int dia_6) {
        this.dia_6 = dia_6;
    }

    public int getDia_7() {
        return dia_7;
    }

    public void setDia_7(int dia_7) {
        this.dia_7 = dia_7;
    }

    public int getDia_8() {
        return dia_8;
    }

    public void setDia_8(int dia_8) {
        this.dia_8 = dia_8;
    }

    public int getDia_9() {
        return dia_9;
    }

    public void setDia_9(int dia_9) {
        this.dia_9 = dia_9;
    }

    public int getDia_10() {
        return dia_10;
    }

    public void setDia_10(int dia_10) {
        this.dia_10 = dia_10;
    }

    public int getDia_11() {
        return dia_11;
    }

    public void setDia_11(int dia_11) {
        this.dia_11 = dia_11;
    }

    public int getDia_12() {
        return dia_12;
    }

    public void setDia_12(int dia_12) {
        this.dia_12 = dia_12;
    }

    public int getDia_13() {
        return dia_13;
    }

    public void setDia_13(int dia_13) {
        this.dia_13 = dia_13;
    }

    public int getDia_14() {
        return dia_14;
    }

    public void setDia_14(int dia_14) {
        this.dia_14 = dia_14;
    }

    public int getDia_15() {
        return dia_15;
    }

    public void setDia_15(int dia_15) {
        this.dia_15 = dia_15;
    }

    public int getDia_16() {
        return dia_16;
    }

    public void setDia_16(int dia_16) {
        this.dia_16 = dia_16;
    }

    public int getDia_17() {
        return dia_17;
    }

    public void setDia_17(int dia_17) {
        this.dia_17 = dia_17;
    }

    public int getDia_18() {
        return dia_18;
    }

    public void setDia_18(int dia_18) {
        this.dia_18 = dia_18;
    }

    public int getDia_19() {
        return dia_19;
    }

    public void setDia_19(int dia_19) {
        this.dia_19 = dia_19;
    }

    public int getDia_20() {
        return dia_20;
    }

    public void setDia_20(int dia_20) {
        this.dia_20 = dia_20;
    }

    public int getDia_21() {
        return dia_21;
    }

    public void setDia_21(int dia_21) {
        this.dia_21 = dia_21;
    }

    public int getDia_22() {
        return dia_22;
    }

    public void setDia_22(int dia_22) {
        this.dia_22 = dia_22;
    }

    public int getDia_23() {
        return dia_23;
    }

    public void setDia_23(int dia_23) {
        this.dia_23 = dia_23;
    }

    public int getDia_24() {
        return dia_24;
    }

    public void setDia_24(int dia_24) {
        this.dia_24 = dia_24;
    }

    public int getDia_25() {
        return dia_25;
    }

    public void setDia_25(int dia_25) {
        this.dia_25 = dia_25;
    }

    public int getDia_26() {
        return dia_26;
    }

    public void setDia_26(int dia_26) {
        this.dia_26 = dia_26;
    }

    public int getDia_27() {
        return dia_27;
    }

    public void setDia_27(int dia_27) {
        this.dia_27 = dia_27;
    }

    public int getDia_28() {
        return dia_28;
    }

    public void setDia_28(int dia_28) {
        this.dia_28 = dia_28;
    }

    public int getDia_29() {
        return dia_29;
    }

    public void setDia_29(int dia_29) {
        this.dia_29 = dia_29;
    }

    public int getDia_30() {
        return dia_30;
    }

    public void setDia_30(int dia_30) {
        this.dia_30 = dia_30;
    }

    public int getDia_31() {
        return dia_31;
    }

    public void setDia_31(int dia_31) {
        this.dia_31 = dia_31;
    }

    public int getNro_horas_totales() {
        return nro_horas_totales;
    }

    public void setNro_horas_totales(int nro_horas_totales) {
        this.nro_horas_totales = nro_horas_totales;
    }

    public int getId_programacion_medica() {
        return id_programacion_medica;
    }

    public void setId_programacion_medica(int id_programacion_medica) {
        this.id_programacion_medica = id_programacion_medica;
    }
    
    
    
    
}
