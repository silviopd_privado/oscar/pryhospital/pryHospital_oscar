package util;

import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.type.OrientationEnum;
import net.sf.jasperreports.swing.JRViewer;

public class Reportes extends accesoDatos.Conexion{ //hce herencia de la clase conexion
    
    public static final String RUTA_REPORTES = System.getProperties().getProperty("user.dir")+"/src/reporte/"; 
    // el RUTA_REPORTES hace referencia de donde esta grabado la informacion en donde dentro de mi proyecto hay una carpeta reportes de ahi va a tomar la carpeta reportes
    
    
    public JRViewer reporteInterno(String archivoReporte, Map<String,Object> parametros) throws Exception{
        //parametros: nombre del archivo el reporte, y tambien va a pedir dos parametros ... el MAP sirve para enviar dentro de una sola variable varios parametros..
        try {
            //URL rutaR = new URL(getClass().getResource("/reportes/"+archivoReporte).toString());
            //JasperPrint reporte = JasperFillManager.fillReport(rutaR.getPath(), parametros, this.abrirConexion());
            JasperPrint reporte = JasperFillManager.fillReport(Reportes.RUTA_REPORTES + archivoReporte, parametros, this.abrirConexion());
            JRViewer visor =new JRViewer(reporte);
            return visor;
            
        }catch(net.sf.jasperreports.engine.JRException e1){ // errores del java
            Funciones.mensajeError(e1.getMessage(), Funciones.NOMBRE_SOFTWARE);
            
        }catch(org.postgresql.util.PSQLException e3){ // errores de pg admin
            Funciones.mensajeError(e3.getMessage(), Funciones.NOMBRE_SOFTWARE);
            
        }catch(Exception e2){ // errores generales
            Funciones.mensajeError(e2.getMessage(), Funciones.NOMBRE_SOFTWARE);
        
        }
        
        return null;
        
    }

    
    public JRViewer reporteInternoHorizontal(String archivoReporte, Map<String,Object> parametros) throws Exception{
        try {
            //URL rutaR = new URL(getClass().getResource("/reportes/"+archivoReporte).toString());
            //JasperPrint reporte = JasperFillManager.fillReport(rutaR.getPath(), parametros, this.abrirConexion());
            JasperPrint reporte = JasperFillManager.fillReport(Reportes.RUTA_REPORTES + archivoReporte, parametros, this.abrirConexion());
            JRViewer visor =new JRViewer(reporte);
            
            //OrientationEnum.LANDSCAPE = Horizontal
            reporte.setOrientation(OrientationEnum.LANDSCAPE);
            return visor;
            
        } catch (JRException e ) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        
        return null;
        
    }
}
